from django.contrib import admin
from .models import Hq, Worker


admin.site.register(Hq)
admin.site.register(Worker)