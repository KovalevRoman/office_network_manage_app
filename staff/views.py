from django.shortcuts import render
from django.contrib import messages
from django.http import Http404
from django.views.generic import CreateView, ListView, UpdateView, DeleteView
from django.urls import reverse_lazy
from .models import Hq, Worker
from .forms import HqCreateForm, WorkerCreateForm

# Create your views here.

class HqUpdateView(UpdateView):
    form_class = HqCreateForm
    template_name = "staff/shtab-update-details.html"

    def post(self, request, *args, **kwargs):
        super(HqUpdateView, self).post(request, *args, **kwargs)
        action = request.POST.get('action')
        worker_pk = request.POST.getlist('worker_pk')
        print(action)
        if action == 'transfer':
            to_hq = request.POST.get('to_hq').split(' ')[0]
            for pk in worker_pk:
                obj = Worker.objects.get(pk=pk)
                obj.hq = Hq.objects.get(pk=to_hq)
                obj.save()

        if action == 'remove':
            for pk in worker_pk:
                Worker.objects.get(pk=pk).delete()

        return super(HqUpdateView, self).post(request, *args, **kwargs)

    def get_queryset(self):
        return Hq.objects.filter(pk=self.kwargs.get('pk'))

    def get_context_data(self, **kwargs):
        context = super(HqUpdateView, self).get_context_data(**kwargs)
        context['all_hq'] = Hq.objects.all()
        return context


class HqListView(ListView):
    model = Hq


class HqDeleteView(DeleteView):
    model = Hq
    success_url = reverse_lazy("shtab-list")
    def get_queryset(self):
        pk = self.kwargs['pk']
        return self.model.objects.filter(pk=pk)

class HqCreateView(CreateView):
    model = Hq
    form_class = HqCreateForm



class WorkerCreateView(CreateView):
    model = Worker
    form_class = WorkerCreateForm


class WorkerUpdateView(UpdateView):
    form_class = WorkerCreateForm
    template_name = "staff/worker-update-details.html"

    def get_queryset(self):
        return Worker.objects.filter(pk=self.kwargs.get('pk'))


class WorkerListView(ListView):
    model = Worker

class WorkerDeleteView(DeleteView):
    model = Worker
    success_url = reverse_lazy("workers-list")
    def get_queryset(self):
        pk = self.kwargs['pk']
        return self.model.objects.filter(pk=pk)



