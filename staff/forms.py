from django.forms import ModelForm, Textarea, TextInput

from .models import Hq, Worker

class HqCreateForm(ModelForm):
    class Meta:
        model = Hq
        fields = [
            "name",
            "address",
        ]
        widgets = {
            'name': TextInput(attrs={'class':"form-control"}),
            'address': TextInput(attrs={'class': "form-control"})
        }


class WorkerCreateForm(ModelForm):
    class Meta:
        model = Worker
        fields = [
            "name",
            "role",
            "hq",
        ]
        widgets = {
            'name': TextInput(attrs={'class':"form-control"}),
            'role': TextInput(attrs={'class': "form-control"}),
            'hq': TextInput(attrs={'class': "form-control"})
        }