from django.db import models
from django.conf import settings
from django.urls import reverse

USER = settings.AUTH_USER_MODEL

# Create your models here.
class Hq(models.Model):
    name = models.CharField(max_length=50, blank=False, verbose_name='Название штаба')
    address = models.CharField(max_length=150, blank=True, verbose_name='Адрес штаба')

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('shtab-detail', kwargs={"pk":self.pk})



class Worker(models.Model):
    name = models.CharField(max_length=100, blank=False, verbose_name='ФИО сотрудника')

    ROLE_HEAD = 'hd'
    ROLE_NEWSMAKER = 'nm'
    ROLE_LAWYER = 'lw'

    ROLE_CHOICES = (
        (ROLE_HEAD, 'Руководитель'),
        (ROLE_NEWSMAKER, 'Новостник'),
        (ROLE_LAWYER, 'Юрист'),
    )

    role = models.CharField(max_length=2, choices=ROLE_CHOICES, verbose_name='Роль сотрудника')
    hq = models.ForeignKey(Hq, on_delete=models.SET_NULL, null=True, blank=True, default='', related_name='hq', verbose_name='Штаб сотрудника')

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('worker-detail', kwargs={"pk":self.pk})