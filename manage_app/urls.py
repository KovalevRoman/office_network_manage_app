"""manage_app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path
from django.contrib.auth.views import LoginView, LogoutView

from staff.views import (
    HqCreateView, HqListView, HqUpdateView, HqDeleteView,
    WorkerListView, WorkerCreateView, WorkerUpdateView, WorkerDeleteView
    )
from news.views import NewsListView, NewsCreateView, NewsUpdateView


urlpatterns = [
    path('admin/', admin.site.urls),
    path('shtabs/create_shtab/', HqCreateView.as_view(), name = 'create-shtab'),
    path('shtabs/', HqListView.as_view(), name = 'shtab-list'),
    re_path(r'^shtabs/(?P<pk>\d+)/$', HqUpdateView.as_view(), name='shtab-detail'),
    re_path(r'^shtabs/(?P<pk>\d+)/del$', HqDeleteView.as_view(), name='close-shtab'),

    path('workers/', WorkerListView.as_view(), name='workers-list'),
    path('workers/create_worker/', WorkerCreateView.as_view(), name='create-worker'),
    re_path(r'^workers/(?P<pk>\d+)/$', WorkerUpdateView.as_view(), name='worker-detail'),
    re_path(r'^workers/(?P<pk>\d+)/del$', WorkerDeleteView.as_view(), name='layoff-worker'),

    path('', NewsListView.as_view(), name='mainpage'),
    path('news/create_new/', NewsCreateView.as_view(), name='create-new'),
    re_path(r'^news/(?P<pk>\d+)/$', NewsUpdateView.as_view(), name='news-detail'),

    path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout')
]
