from django.shortcuts import render
from django.views.generic import ListView, CreateView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin

from news.models import News
from .forms import NewsCreateForm
# Create your views here.

class NewsListView(ListView):
    model = News

    def get_queryset(self):
        return News.objects.order_by('-date')

class NewsCreateView(LoginRequiredMixin, CreateView):
    model = News
    form_class = NewsCreateForm

class NewsUpdateView(LoginRequiredMixin, UpdateView):
    form_class = NewsCreateForm
    template_name = "news/new-update-details.html"

    def get_queryset(self):
        return News.objects.filter(pk=self.kwargs.get('pk'))