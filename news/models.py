from django.db import models
from django.urls import reverse
from staff.models import Hq

# Create your models here.
class News(models.Model):
    published = models.BooleanField(default=False)
    date = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')
    content = models.TextField(blank=False, max_length=1000, verbose_name='Контент новости')
    header = models.CharField(blank=False, max_length=100, verbose_name='Заголовок новости')
    shtab = models.ForeignKey(
        Hq,
        null=True,
        blank=True,
        on_delete=None,
        related_name="shtab",
        verbose_name='Штаб новости'
    )

    def __str__(self):
        return self.header

    def get_absolute_url(self):
        return reverse('news-detail', kwargs={"pk":self.pk})