from django.forms import ModelForm, Textarea, TextInput, Select

from news.models import News

class NewsCreateForm(ModelForm):
    class Meta:
        model = News
        fields = [
            "header",
            "content",
            "shtab",
        ]
        widgets = {
            'header': TextInput(attrs={'class':"form-control"}),
            'content': Textarea(attrs={'class': "form-control"}),
            'shtab': Select(attrs={'class': "form-control"})
        }